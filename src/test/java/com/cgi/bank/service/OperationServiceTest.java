package com.cgi.bank.service;


import com.cgi.bank.bo.BankAccount;
import com.cgi.bank.bo.Operation;
import com.cgi.bank.bo.UserEntity;
import com.cgi.bank.exceptions.AccountNotFoundException;
import com.cgi.bank.exceptions.InsuffisantBalanceException;
import com.cgi.bank.exceptions.TransactionException;
import com.cgi.bank.mapper.OperationMapper;
import com.cgi.bank.repository.AccountRepository;
import com.cgi.bank.repository.OperationRepository;
import com.cgi.bank.services.AuditService;
import com.cgi.bank.services.OperationServiceImpl;
import com.cgi.bank.utils.OperationType;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class OperationServiceTest {

    private Operation operation;
    private BankAccount bankAccount1;
    private UserEntity appUser1;


    @InjectMocks
    private OperationServiceImpl operationService;

    @Mock
    AuditService auditService;
    @Mock
    OperationRepository operationRepository;
    @Mock
    AccountRepository accountRepository;



    @BeforeEach
    void initialize(){
        appUser1 = new UserEntity();
        appUser1.setUsername("user1");
        appUser1.setLastname("last1");
        appUser1.setFirstname("first1");
        appUser1.setGender("Male");

        bankAccount1 = new BankAccount();
        bankAccount1.setId(1L);
        bankAccount1.setAccountNumber("010000A000001000");
        bankAccount1.setRib("RIB1");
        bankAccount1.setBalance(BigDecimal.valueOf(200000L));
        bankAccount1.setUser(appUser1);


        operation = new Operation();
        operation.setReason("Depot d'argent pour urgence !");
        operation.setAmount(BigDecimal.valueOf(2000L));
        operation.setOperationAccount(bankAccount1);
        operation.setDateExecution(new Date());
        operation.setOperationType(OperationType.WITHDRAWAL);
        operation.setOperationAuthorName("Aboudou");

        System.out.println(operation.getOperationType());

    }

    @AfterEach
    void destroy(){
        bankAccount1 =null;
        appUser1=null;
        operation = null;
    }



    // Withdrawals

    @DisplayName("It Should raise a AccountNotFoundException since no account is given")
    @Test
    public void testWithdrawalAccountNotFoundException(){
        when(accountRepository.findByAccountNumber(any())).thenReturn(null);

        AccountNotFoundException accountNotFoundException = assertThrows(AccountNotFoundException.class,()->{
            operationService.createWithdrawal(OperationMapper.mapModelToDto(operation));
        });

        assertEquals(accountNotFoundException.getMessage(),"Compte Non existant");

    }

    @DisplayName("It Should raise a TransactionException since the withdrew money is higher than the maximum set")
    @Test
    public void testWithdrawalTransactionExceedTransactionMoneyException(){
        when(accountRepository.findByAccountNumber(any())).thenReturn(bankAccount1);
        operation.setAmount(BigDecimal.valueOf(2000000L));
        TransactionException transactionException = assertThrows(TransactionException.class,()->{
            operationService.createWithdrawal(OperationMapper.mapModelToDto(operation));
        });

        assertEquals(transactionException.getMessage(),"Montant maximal dépassé");

    }

    @DisplayName("It Should raise an InsuffisantException since the sender's money is less than the transaction money")
    @Test
    public void testTransferInsuffisantBalanceException(){
        bankAccount1.setBalance(BigDecimal.valueOf(0));

        when(accountRepository.findByAccountNumber("010000A000001000")).thenReturn(bankAccount1);

        InsuffisantBalanceException transactionException = assertThrows(InsuffisantBalanceException.class,()->{
                operationService.createWithdrawal(OperationMapper.mapModelToDto(operation));
        });

        assertEquals(transactionException.getMessage(),"Solde insuffisant");

    }



    @DisplayName("It Should raise a TransactionException since the transaction reason is not given")
    @Test
    public void testWithdrawalTransactionMotifException(){
        when(accountRepository.findByAccountNumber(any())).thenReturn(bankAccount1);
        operation.setReason("");

        TransactionException transactionException = assertThrows(TransactionException.class,()->{
            operationService.createWithdrawal(OperationMapper.mapModelToDto(operation));
        });

        assertEquals(transactionException.getMessage(),"Motif vide");

    }


    @DisplayName("It Should decrease the amount of money withdrew from the account")
    @Test
    public void testWithdrawalShouldDecreaseFromAmount() throws Exception{
        when(accountRepository.findByAccountNumber(any())).thenReturn(bankAccount1);

        BigDecimal account1ExpectedAmount = bankAccount1.getBalance().subtract(operation.getAmount());

        operationService.createWithdrawal(OperationMapper.mapModelToDto(operation));
        assertEquals(account1ExpectedAmount,operation.getOperationAccount().getBalance());
    }





    // Deposits


    @DisplayName("It Should raise an AccountNotFoundException since no account is given")
    @Test
    public void testDepositAccountNotFoundException(){
        when(accountRepository.findByAccountNumber(any())).thenReturn(null);

        AccountNotFoundException accountNotFoundException = assertThrows(AccountNotFoundException.class,()->{
            operationService.createDeposit(OperationMapper.mapModelToDto(operation));
        });

        assertEquals(accountNotFoundException.getMessage(),"Compte Non existant");

    }

    @DisplayName("It Should raise an TransactionException since the transaction money is higher than the maximum set")
    @Test
    public void testDepositTransactionExceedTransactionMoneyException(){
        when(accountRepository.findByAccountNumber(any())).thenReturn(bankAccount1);
        operation.setAmount(BigDecimal.valueOf(2000000L));
        TransactionException transactionException = assertThrows(TransactionException.class,()->{
            operationService.createDeposit(OperationMapper.mapModelToDto(operation));
        });

        assertEquals(transactionException.getMessage(),"Montant maximal dépassé");

    }


    @DisplayName("It Should raise an TransactionException since the transaction authorname is not given")
    @Test
    public void testDepositTransactionEmptyAuthorNameException(){
        when(accountRepository.findByAccountNumber(any())).thenReturn(bankAccount1);

        operation.setOperationAuthorName("");

        TransactionException transactionException = assertThrows(TransactionException.class,()->{
            operationService.createDeposit(OperationMapper.mapModelToDto(operation));
        });

        assertEquals(transactionException.getMessage(),"Nom de l'auteur de l'opération non fourni");

    }

    @Test
    public void testDepositShouldIncreaseRecieverAmount() throws Exception{
        when(accountRepository.findByAccountNumber(any())).thenReturn(bankAccount1);

        BigDecimal account1ExpectedAmount = bankAccount1.getBalance().add(operation.getAmount());

        operationService.createDeposit(OperationMapper.mapModelToDto(operation));
        assertEquals(account1ExpectedAmount,operation.getOperationAccount().getBalance());
    }





}
