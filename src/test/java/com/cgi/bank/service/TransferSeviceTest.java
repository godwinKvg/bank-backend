package com.cgi.bank.service;

import com.cgi.bank.bo.BankAccount;
import com.cgi.bank.bo.Transfer;
import com.cgi.bank.bo.UserEntity;
import com.cgi.bank.exceptions.AccountNotFoundException;
import com.cgi.bank.exceptions.InsuffisantBalanceException;
import com.cgi.bank.exceptions.TransactionException;
import com.cgi.bank.mapper.TransferMapper;
import com.cgi.bank.repository.AccountRepository;
import com.cgi.bank.repository.TransferRepository;
import com.cgi.bank.services.AuditService;
import com.cgi.bank.services.TransferSeviceImpl;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class TransferSeviceTest {

    private Transfer transfer;
    private BankAccount bankAccount1;
    private BankAccount bankAccount2;

    private UserEntity appUser1;
    private UserEntity appUser2;


    @InjectMocks
    private TransferSeviceImpl transferService;

    @Mock
    TransferRepository transferRepository;
    @Mock
    AuditService auditService;
    @Mock
    AccountRepository accountRepository;


    @BeforeEach
    void initialize(){
        appUser1 = new UserEntity();
        appUser1.setUsername("user1");
        appUser1.setLastname("last1");
        appUser1.setFirstname("first1");
        appUser1.setGender("Male");


        appUser2 = new UserEntity();
        appUser2.setUsername("user2");
        appUser2.setLastname("last2");
        appUser2.setFirstname("first2");
        appUser2.setGender("Female");


        bankAccount1 = new BankAccount();
        bankAccount1.setId(1L);
        bankAccount1.setAccountNumber("010000A000001000");
        bankAccount1.setRib("RIB1");
        bankAccount1.setBalance(BigDecimal.valueOf(20000));
        bankAccount1.setUser(appUser1);


        bankAccount2 = new BankAccount();
        bankAccount2.setId(2L);
        bankAccount2.setAccountNumber("010000B025001000");
        bankAccount2.setRib("RIB2");
        bankAccount2.setBalance(BigDecimal.valueOf(14000));
        bankAccount2.setUser(appUser2);


        transfer = new Transfer();
        transfer.setReason("Transfer d'argent pour une urgence !");
        transfer.setAmount(BigDecimal.valueOf(2000));
        transfer.setReceiverBankAccount(bankAccount2);
        transfer.setSenderBankAccount(bankAccount1);
        transfer.setDateExecution(new Date());

    }

    @AfterEach
    void destroy(){
        bankAccount1 =null;
        bankAccount2 =null;

        appUser1=null;
        appUser2=null;
    }

    @DisplayName("It Should raise an AccountNotFoundException since the sender account is not porvided")
    @Test
    public void testTransferSenderAccountNotFoundException(){

        when(accountRepository.findByAccountNumber("010000A000001000")).thenReturn(null);

        AccountNotFoundException accountNotFoundException = assertThrows(AccountNotFoundException.class,()->{
            transferService.createTransfer(TransferMapper.mapModelToDto(transfer));
        });

        assertEquals(accountNotFoundException.getMessage(),"Compte de l'emetteur Non existant");

    }

    @DisplayName("It Should raise an AccountNotFoundException since the reciever account is null")
    @Test
    public void testTransferRecieverAccountNotFoundException(){

        when(accountRepository.findByAccountNumber("010000A000001000")).thenReturn(bankAccount1);
        when(accountRepository.findByAccountNumber("010000B025001000")).thenReturn(null);

        AccountNotFoundException accountNotFoundException = assertThrows(AccountNotFoundException.class,()->{
            transferService.createTransfer(TransferMapper.mapModelToDto(transfer));
        });

        assertEquals(accountNotFoundException.getMessage(),"Compte du receveur Non existant");

    }

    @DisplayName("It Should raise an TransactionException since the transaction money is zero or null")
    @Test
    public void testTransferZeroMoneyException(){
        when(accountRepository.findByAccountNumber("010000A000001000")).thenReturn(bankAccount1);
        when(accountRepository.findByAccountNumber("010000B025001000")).thenReturn(bankAccount2);

        transfer.setAmount(BigDecimal.valueOf(0));

        TransactionException transactionException = assertThrows(TransactionException.class,()->{
            transferService.createTransfer(TransferMapper.mapModelToDto(transfer));
        });

        assertEquals(transactionException.getMessage(),"Montant vide");

    }

    @DisplayName("It Should raise an TransactionException since the transaction money is less than the minimum value")
    @Test
    public void testTransferInsuffiscientMoneyException(){
        when(accountRepository.findByAccountNumber("010000A000001000")).thenReturn(bankAccount1);
        when(accountRepository.findByAccountNumber("010000B025001000")).thenReturn(bankAccount2);

        transfer.setAmount(BigDecimal.valueOf(2));

        TransactionException transactionException = assertThrows(TransactionException.class,()->{
            transferService.createTransfer(TransferMapper.mapModelToDto(transfer));
        });

        assertEquals(transactionException.getMessage(),"Montant minimal de transfer non atteint");

    }

    @DisplayName("It Should raise an TransactionException since the transaction money is greater than the maximum barrier")
    @Test
    public void testTransferMaximumMoneyReachedException(){
        when(accountRepository.findByAccountNumber("010000A000001000")).thenReturn(bankAccount1);
        when(accountRepository.findByAccountNumber("010000B025001000")).thenReturn(bankAccount2);

        transfer.setAmount(BigDecimal.valueOf(2000000));

        TransactionException transactionException = assertThrows(TransactionException.class,()->{
            transferService.createTransfer(TransferMapper.mapModelToDto(transfer));
        });

        assertEquals(transactionException.getMessage(),"Montant maximal de transfer dépassé");

    }


    @DisplayName("It Should raise an InsuffisantException since the sender's money is less than the transaction money")
    @Test
    public void testTransferInsuffisantBalanceException(){
        bankAccount1.setBalance(BigDecimal.valueOf(300));

        when(accountRepository.findByAccountNumber("010000A000001000")).thenReturn(bankAccount1);
        when(accountRepository.findByAccountNumber("010000B025001000")).thenReturn(bankAccount2);

        InsuffisantBalanceException transactionException = assertThrows(InsuffisantBalanceException.class,()->{
            transferService.createTransfer(TransferMapper.mapModelToDto(transfer));
        });

        assertEquals(transactionException.getMessage(),"Solde insuffisant");

    }


    @DisplayName("It Should raise an TransactionException since the transaction motif is not given")
    @Test
    public void testTransferTransactionMotifException(){
        when(accountRepository.findByAccountNumber("010000A000001000")).thenReturn(bankAccount1);
        when(accountRepository.findByAccountNumber("010000B025001000")).thenReturn(bankAccount2);

        transfer.setReason(null);

        TransactionException transactionException = assertThrows(TransactionException.class,()->{
            transferService.createTransfer(TransferMapper.mapModelToDto(transfer));
        });

        assertEquals(transactionException.getMessage(),"Motif vide");

    }


    @DisplayName("It Should reduce the amount of money transfered from the sender and add it to the reciever")
    @Test
    public void testTransferHasBeenSuccessfull(){
        when(accountRepository.findByAccountNumber("010000A000001000")).thenReturn(bankAccount1);
        when(accountRepository.findByAccountNumber("010000B025001000")).thenReturn(bankAccount2);

        try {
            BigDecimal expectedAccount1Sold = bankAccount1.getBalance().subtract(transfer.getAmount());
            BigDecimal expectedAccount2Sold = bankAccount2.getBalance().add(transfer.getAmount());
            transferService.createTransfer(TransferMapper.mapModelToDto(transfer));
            assertEquals(expectedAccount1Sold, bankAccount1.getBalance());
            assertEquals(expectedAccount2Sold, bankAccount2.getBalance());
        }
        catch (InsuffisantBalanceException | AccountNotFoundException | TransactionException e) {
            throw new RuntimeException(e);
        }


    }



}