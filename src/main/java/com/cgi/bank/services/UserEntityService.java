package com.cgi.bank.services;
import com.cgi.bank.bo.Role;
import com.cgi.bank.bo.UserEntity;
import com.cgi.bank.dto.UserEntityDto;
import com.cgi.bank.dto.UserEntityListDto;
import com.cgi.bank.exceptions.UserAlreadyExistsException;

import java.util.List;



public interface UserEntityService {

    UserEntity createUser(UserEntity u) throws UserAlreadyExistsException;

    Role addNewRole(Role r);

    void addRoleToUser(String username,String rolename);

    UserEntity loadUserByUsername(String username);

    List<UserEntity> listUsers();

    UserEntity updateUser(UserEntityDto u);

    UserEntity changePassword(UserEntity u);

    UserEntity getUserById(Long id);

    UserEntityListDto getUsers(int page, int size);
}