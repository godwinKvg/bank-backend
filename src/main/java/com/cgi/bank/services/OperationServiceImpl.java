package com.cgi.bank.services;

import com.cgi.bank.bo.BankAccount;
import com.cgi.bank.bo.Operation;
import com.cgi.bank.dto.BankAccountDto;
import com.cgi.bank.dto.BankAccountListDTO;
import com.cgi.bank.dto.OperationDto;
import com.cgi.bank.dto.OperationListDTO;
import com.cgi.bank.exceptions.AccountNotFoundException;
import com.cgi.bank.exceptions.InsuffisantBalanceException;
import com.cgi.bank.exceptions.TransactionException;
import com.cgi.bank.mapper.BankAccountMapper;
import com.cgi.bank.mapper.OperationMapper;
import com.cgi.bank.repository.AccountRepository;
import com.cgi.bank.repository.OperationRepository;
import com.cgi.bank.utils.OperationType;
import com.cgi.bank.utils.TransactionValidator;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Transactional
@Service
public class OperationServiceImpl implements OperationService {

    private final OperationRepository operationRepository;
    AuditService auditService;
    AccountRepository accountRepository;

    public OperationServiceImpl(AuditService auditService, OperationRepository operationRepository, AccountRepository accountRepository) {
        this.auditService = auditService;
        this.accountRepository = accountRepository;
        this.operationRepository = operationRepository;
    }

    @Override
    public void createWithdrawal(OperationDto operationDto) throws AccountNotFoundException, InsuffisantBalanceException, TransactionException {
        BankAccount bankAccount = accountRepository.findByAccountNumber(operationDto.getAccountNumber());

        TransactionValidator.validateOperation(operationRepository.countAllByOperationAccountAndDateExecution(bankAccount,new Date()), bankAccount,operationDto);

        bankAccount.setBalance(bankAccount.getBalance().subtract(operationDto.getAmount()));

        Operation operation = OperationMapper.mapDtoToModel(operationDto, bankAccount);

        operation.setOperationType(OperationType.WITHDRAWAL);


        operationRepository.save(operation);
    }

    @Override
    public void createDeposit(OperationDto operationDto) throws AccountNotFoundException, TransactionException,InsuffisantBalanceException {
        BankAccount bankAccount = accountRepository.findByAccountNumber(operationDto.getAccountNumber());

        TransactionValidator.validateOperation(operationRepository.countAllByOperationAccountAndDateExecution(bankAccount,new Date()), bankAccount,operationDto);

        bankAccount.setBalance(bankAccount.getBalance().add(operationDto.getAmount()));

        Operation operation = OperationMapper.mapDtoToModel(operationDto, bankAccount);

        operation.setOperationAuthorName(operationDto.getOperationAuthorName());
        operation.setOperationType(OperationType.DEPOSIT);

        operationRepository.save(operation);
    }

    @Override
    public void getWithdrawal(OperationDto operationDto) throws AccountNotFoundException {

    }

    @Override
    public void getDeposit(OperationDto operationDto) throws AccountNotFoundException {

    }

    @Override
    @PostAuthorize("hasAuthority('ADMIN')")
    public List<Operation> getAll() {
        return operationRepository.findAll();
        }

    @Override
    public OperationListDTO getMyOperations(String receiverBankAccountNumber, int page, int size) {

        OperationListDTO operationListDTO = new OperationListDTO();
        Page<Operation> operations = operationRepository.getAllByOperationAccount_AccountNumberOrderByDateExecutionDesc(receiverBankAccountNumber, PageRequest.of(page, size));

        List<OperationDto> operationDtos = operations.getContent().stream().map(OperationMapper::mapModelToDto).toList();
        operationListDTO.setOperationDtos(operationDtos);
        operationListDTO.setCurrentPage(page);
        operationListDTO.setPageSize(size);
        operationListDTO.setTotalPages(operations.getTotalPages());

        return operationListDTO;
    }

    @Override
    public List<Operation> getDeposits() {
        return operationRepository.getAllByOperationType(OperationType.DEPOSIT);
    }

    @Override
    public List<Operation> getWithdrawals() {
        return operationRepository.getAllByOperationType(OperationType.WITHDRAWAL);
    }

    @Override
    public List<Operation> getWithdrawalsByAccountNumber(String accountNumber) {
        return operationRepository.findAllByOperationAccount_AccountNumberAndOperationType(accountNumber, OperationType.WITHDRAWAL);
    }

    @Override
    public List<Operation> getMyWithdrawals(String username) {
        return operationRepository.getAllByOperationTypeAndOperationAccount_User_Username(OperationType.WITHDRAWAL,username );
    }
}
