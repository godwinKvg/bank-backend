package com.cgi.bank.services;

import com.cgi.bank.bo.Transfer;
import com.cgi.bank.dto.TransferDto;
import com.cgi.bank.dto.TransferListDTO;
import com.cgi.bank.exceptions.AccountNotFoundException;
import com.cgi.bank.exceptions.InsuffisantBalanceException;
import com.cgi.bank.exceptions.TransactionException;

import java.util.List;


public interface TransferService {

    TransferListDTO getMyTransfersByAccountNumber(String accountNumber, int page, int size);

    List<Transfer> findAll();
    void createTransfer(TransferDto transferDto) throws InsuffisantBalanceException, AccountNotFoundException, TransactionException;
}
