package com.cgi.bank.services;

import com.cgi.bank.bo.BankAccount;
import com.cgi.bank.bo.Transfer;
import com.cgi.bank.dto.TransferDto;
import com.cgi.bank.dto.TransferListDTO;
import com.cgi.bank.exceptions.AccountNotFoundException;
import com.cgi.bank.exceptions.InsuffisantBalanceException;
import com.cgi.bank.exceptions.TransactionException;
import com.cgi.bank.mapper.TransferMapper;
import com.cgi.bank.repository.AccountRepository;
import com.cgi.bank.repository.TransferRepository;
import com.cgi.bank.utils.TransactionValidator;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;


@Transactional
@Service
public class TransferSeviceImpl implements TransferService{

    AuditService auditService;
    TransferRepository transferRepository;
    AccountRepository accountRepository;

    public TransferSeviceImpl(AuditService auditService, TransferRepository transferRepository, AccountRepository accountRepository) {
        this.auditService = auditService;
        this.transferRepository = transferRepository;
        this.accountRepository = accountRepository;
    }

    @Override
    public TransferListDTO getMyTransfersByAccountNumber(String accountNumber, int page, int size) {

        TransferListDTO transferListDTO = new TransferListDTO();

        Page<Transfer> transfers = transferRepository.getAllBySenderBankAccount_AccountNumberOrReceiverBankAccount_AccountNumberOrderByDateExecutionDesc(accountNumber, accountNumber, PageRequest.of(page, size));
        List<TransferDto> transferDtos = transfers.getContent().stream().map(TransferMapper::mapModelToDto).toList();

        transferListDTO.setAccountNumber(accountNumber);
        transferListDTO.setTransferDtos(transferDtos);
        transferListDTO.setTotalPages(transfers.getTotalPages());
        transferListDTO.setPageSize(size);
        transferListDTO.setCurrentPage(page);
        return transferListDTO;
    }

    @Override
    @PostAuthorize("hasAuthority('ADMIN')")
    public List<Transfer> findAll(){
        return  transferRepository.findAll();
    }



    public void createTransfer(TransferDto transferDto)
            throws InsuffisantBalanceException, AccountNotFoundException, TransactionException {

        BankAccount senderBankAccount = accountRepository.findByAccountNumber(transferDto.getSenderAccountNumber());
        BankAccount recieverBankAccount = accountRepository.findByAccountNumber(transferDto.getReceiverAccountNumber());

        TransactionValidator.validateTransfert(senderBankAccount, recieverBankAccount,transferDto);

        senderBankAccount.setBalance(senderBankAccount.getBalance().subtract(transferDto.getAmount()));

        recieverBankAccount.setBalance(new BigDecimal(recieverBankAccount.getBalance().intValue() + transferDto.getAmount().intValue()));

        Transfer transfer = TransferMapper.mapDtoToModel(transferDto, recieverBankAccount, senderBankAccount);

        transferRepository.save(transfer);

        auditService.transfer(transferDto);
    }
}
