package com.cgi.bank.services;

import com.cgi.bank.bo.Operation;
import com.cgi.bank.dto.OperationDto;
import com.cgi.bank.dto.OperationListDTO;
import com.cgi.bank.exceptions.AccountNotFoundException;
import com.cgi.bank.exceptions.InsuffisantBalanceException;
import com.cgi.bank.exceptions.TransactionException;

import java.util.List;

public interface OperationService {
    void createWithdrawal(OperationDto operationDto) throws AccountNotFoundException, InsuffisantBalanceException, TransactionException;
    void createDeposit(OperationDto operationDto) throws AccountNotFoundException, TransactionException, InsuffisantBalanceException;
    void getWithdrawal(OperationDto operationDto) throws AccountNotFoundException;
    void getDeposit(OperationDto operationDto) throws AccountNotFoundException;

    List<Operation> getAll();

    OperationListDTO getMyOperations(String accountNumber, int page, int size);

    List<Operation> getDeposits();

    List<Operation> getWithdrawals();

    List<Operation> getWithdrawalsByAccountNumber(String accountNumber);

    List<Operation> getMyWithdrawals(String username);
}
