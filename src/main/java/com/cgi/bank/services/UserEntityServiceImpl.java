package com.cgi.bank.services;

import com.cgi.bank.bo.UserEntity;
import  com.cgi.bank.bo.Role;
import com.cgi.bank.dto.UserEntityDto;
import com.cgi.bank.dto.UserEntityListDto;
import com.cgi.bank.exceptions.UserAlreadyExistsException;
import com.cgi.bank.mapper.UserEntitMapper;
import com.cgi.bank.repository.RoleRepository;
import com.cgi.bank.repository.UserEntityRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import jakarta.transaction.Transactional;
import java.util.List;

@Transactional
@Service
public class UserEntityServiceImpl implements UserEntityService {

    private static final Logger log = LoggerFactory.getLogger(UserEntityServiceImpl.class);
    private final UserEntityRepository userRepository;
    private final RoleRepository roleRepository;
    private final PasswordEncoder passwordEncoder;

    public UserEntityServiceImpl(UserEntityRepository ur, RoleRepository rr, PasswordEncoder pe) {
        userRepository = ur;
        roleRepository = rr;
        passwordEncoder = pe;
    }

    private UserEntity addNewUser(UserEntity u) {
        String pw = u.getPassword();
        u.setPassword(passwordEncoder.encode(pw));
        return userRepository.save(u);
    }

    @Override
    public UserEntity createUser(UserEntity u)throws UserAlreadyExistsException {
        UserEntity usr = userRepository.findByUsername(u.getUsername());
        if (usr != null) {
            throw new UserAlreadyExistsException();
        }
        UserEntity user = addNewUser(u);
        addRoleToUser(user.getUsername(), "USER");
        log.error("UserEntity created: " + user.getUsername() + " " +user.getRole());
        return user;
    }


    @Override
    public Role addNewRole(Role r) {
        return roleRepository.save(r);
    }

    @Override
    public void addRoleToUser(String username, String rolename) {
        UserEntity u = userRepository.findByUsername(username);
        Role r = roleRepository.findByRoleName(rolename);
        u.setRole(r);
        userRepository.save(u);
    }

    @Override
    public UserEntity loadUserByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    @PostAuthorize("hasAuthority('ADMIN')")
    public List<UserEntity> listUsers() {
        return userRepository.findAll();
    }

    @Override
    public UserEntity updateUser(UserEntityDto u) {

        if(u.getUsername()==null) throw new UsernameNotFoundException("Nom d'utilisateur non existant");

        UserEntity user = loadUserByUsername(u.getUsername());

        if (user == null) throw new UsernameNotFoundException("Utilisateur non trouvé");

        user.setFirstname(u.getFirstname() == null ?  user.getFirstname() : u.getFirstname());
        user.setLastname(u.getLastname() == null ?  user.getLastname() : u.getLastname());
        user.setBirthdate(u.getBirthdate() == null ?  user.getBirthdate() : u.getBirthdate());

        return userRepository.save(user);
    }

    @Override
    public UserEntity changePassword(UserEntity u) {
        UserEntity user = loadUserByUsername(u.getUsername());
        user.setPassword(u.getPassword());
        return userRepository.save(user);
    }

    @Override
    public UserEntity getUserById(Long id) {
        return userRepository.findById(id).isPresent() ? userRepository.findById(id).get() : null;
    }

    @Override
    public UserEntityListDto getUsers(int page, int size) {
        UserEntityListDto usersListDto = new UserEntityListDto();

        Page<UserEntity> users = userRepository.findAll(PageRequest.of(page, size));

        List<UserEntityDto> userEntityDtos = users.getContent().stream().map(UserEntitMapper::mapModelToDto).toList();

        usersListDto.setUsersDto(userEntityDtos);
        usersListDto.setCurrentPage(page);
        usersListDto.setTotalPages(users.getTotalPages());

        return usersListDto;
    }


}

