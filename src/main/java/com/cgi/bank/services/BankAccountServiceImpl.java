package com.cgi.bank.services;

import com.cgi.bank.bo.BankAccount;
import com.cgi.bank.bo.UserEntity;
import com.cgi.bank.dto.BankAccountDto;
import com.cgi.bank.dto.BankAccountListDTO;
import com.cgi.bank.exceptions.AccountNotFoundException;
import com.cgi.bank.exceptions.BankAccountNotFound;
import com.cgi.bank.exceptions.MaximumNumberOfAccountReached;
import com.cgi.bank.mapper.BankAccountMapper;
import com.cgi.bank.repository.AccountRepository;
import com.cgi.bank.utils.AccountUtils;
import com.cgi.bank.utils.RandomStringGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.core.AuthenticatedPrincipal;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class BankAccountServiceImpl implements BankAccountService {

    AccountRepository accountRepository;
    UserEntityService userEntityServiceImpl;

    public BankAccountServiceImpl(AccountRepository accountRepository, UserEntityService userEntityServiceImpl) {
        this.accountRepository = accountRepository;
        this.userEntityServiceImpl = userEntityServiceImpl;
    }

    @Override
    public BankAccount save(BankAccount a) {
        return accountRepository.save(a);
    }

    @Override
    public BankAccount create(BankAccountDto bankAccountDto) throws MaximumNumberOfAccountReached{

        Long numberOfAccounts = accountRepository.countByUser_Username(bankAccountDto.getUsername());

        if (numberOfAccounts > AccountUtils.MAX_ACCOUNT) {
            throw new MaximumNumberOfAccountReached();
        }

        String generateStringForAccountNumber = RandomStringGenerator.generateString();
        BankAccount account = accountRepository.findByAccountNumber(generateStringForAccountNumber);

        UserEntity user = userEntityServiceImpl.loadUserByUsername(bankAccountDto.getUsername());

        while (account != null){
            generateStringForAccountNumber = RandomStringGenerator.generateString();
            account = accountRepository.findByAccountNumber(generateStringForAccountNumber);
        }

        bankAccountDto.setAccountNumber(generateStringForAccountNumber);


        // Setting Rib
        String generateStringForRib = RandomStringGenerator.generateString();
        account = accountRepository.findByRib(generateStringForRib);

        while (account != null){
            generateStringForRib = RandomStringGenerator.generateString();
            account = accountRepository.findByRib(generateStringForRib);
        }

        bankAccountDto.setRib(generateStringForRib);

        if (!user.getRole().getRoleName().equals("ADMIN") || bankAccountDto.getBalance()==null){
            bankAccountDto.setBalance(BigDecimal.ZERO);
        }

        return accountRepository.save(BankAccountMapper.mapDtoToModel(bankAccountDto,user));

    }

    @Override
    public BankAccount getAccount(String accountNumber) throws BankAccountNotFound {
        BankAccount account = accountRepository.findByAccountNumber(accountNumber);
        if (account==null)
            throw new BankAccountNotFound();
        return account;
    }

    @Override
    public BankAccountListDTO getAccountsByUsernameWithPagination(String username, int page, int size) {
        BankAccountListDTO accountListDTO = new BankAccountListDTO();
        Page<BankAccount> accounts = accountRepository.findAllByUser_Username(username, PageRequest.of(page,size));
        List<BankAccountDto> accountDtos = accounts.getContent().stream().map(BankAccountMapper::mapModelToDto).toList();
        accountListDTO.setBankAccountDtos(accountDtos);
        accountListDTO.setCurrentPage(page);
        accountListDTO.setPageSize(size);
        accountListDTO.setTotalPages(accounts.getTotalPages());
        return  accountListDTO;
    }

    @Override
    public List<BankAccount> getAccountsByUsername(String username) {
       return accountRepository.findAllByUser_Username(username);
    }


    @Override
    public BankAccount update(BankAccount bankAccount) throws AccountNotFoundException {
        BankAccount bankAccount1 = null;

        if (bankAccount !=null && bankAccount.getId()!=0){
            bankAccount1 = accountRepository.findByAccountNumber(bankAccount.getAccountNumber());
            bankAccount1.setBalance(bankAccount.getBalance()!=null? bankAccount.getBalance(): bankAccount1.getBalance());
        }else {
            throw new AccountNotFoundException();
        }

        return bankAccount1;

    }


    @Override
    @PostAuthorize("hasAuthority('ADMIN')")
    public BankAccountListDTO getAccounts(int page,int size) {

        BankAccountListDTO bankAccountListDTO = new BankAccountListDTO();
        Page<BankAccount> bankAccounts = accountRepository.findAll(PageRequest.of(page,size));
        List<BankAccountDto> accountDtos = bankAccounts.getContent().stream().map(BankAccountMapper::mapModelToDto).toList();
        bankAccountListDTO.setBankAccountDtos(accountDtos);
        bankAccountListDTO.setCurrentPage(page);
        bankAccountListDTO.setPageSize(size);
        bankAccountListDTO.setTotalPages(bankAccounts.getTotalPages());
        return bankAccountListDTO;
    }
}
