package com.cgi.bank.services;

import com.cgi.bank.bo.Audit;
import com.cgi.bank.dto.OperationDto;
import com.cgi.bank.dto.TransferDto;
import com.cgi.bank.repository.AuditRepository;
import com.cgi.bank.utils.EventType;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class AuditServiceImpl implements AuditService{

    @Autowired
    private AuditRepository auditRepository;

    public void transfer(TransferDto transferDto) {
        Audit audit = new Audit();
        audit.setEventType(EventType.TRANSFER);
        audit.setMessage("Transfer depuis " + transferDto.getSenderAccountNumber() + " vers " + transferDto
                .getReceiverAccountNumber() + " d'un montant de " + transferDto.getAmount()
                .toString());
        auditRepository.save(audit);
    }


    public void deposit(OperationDto operation) {
        Audit audit = new Audit();
        audit.setEventType(EventType.DEPOSIT);
        audit.setMessage("Depot sur le compte " + operation.getAccountNumber() + " par " + operation
                .getOperationAuthorName() + " d'un montant de " + operation.getAmount()
                .toString());
        auditRepository.save(audit);
    }

    @Override
    public void withdrawal(OperationDto operationDto) {
        Audit audit = new Audit();

            audit.setEventType(EventType.DEPOSIT);
            audit.setEventType(EventType.WITHDRAWAL);
        audit.setMessage("Depot sur le compte " + operationDto.getAccountNumber() + " par " + operationDto.getOperationAuthorName() + " d'un montant de " + operationDto.getAmount()
                .toString());
        auditRepository.save(audit);
    }
}
