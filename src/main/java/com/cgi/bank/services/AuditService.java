package com.cgi.bank.services;

import com.cgi.bank.dto.OperationDto;
import com.cgi.bank.dto.TransferDto;

public interface AuditService {
    void transfer(TransferDto transferDto);
    void deposit(OperationDto depositDto);
    void withdrawal(OperationDto operationDto);
}
