package com.cgi.bank.services;

import com.cgi.bank.bo.BankAccount;
import com.cgi.bank.dto.BankAccountDto;
import com.cgi.bank.dto.BankAccountListDTO;
import com.cgi.bank.exceptions.AccountNotFoundException;
import com.cgi.bank.exceptions.BankAccountNotFound;
import com.cgi.bank.exceptions.MaximumNumberOfAccountReached;

import java.util.List;

public interface BankAccountService {
    BankAccountListDTO getAccounts(int page, int size);

    BankAccount save(BankAccount a);

    BankAccount create(BankAccountDto a) throws MaximumNumberOfAccountReached;

    BankAccount getAccount(String accountNumber) throws BankAccountNotFound;

    List<BankAccount> getAccountsByUsername(String username);

    BankAccountListDTO getAccountsByUsernameWithPagination(String username, int page, int size);

    BankAccount update(BankAccount bankAccount) throws AccountNotFoundException;

}
