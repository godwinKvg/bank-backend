package com.cgi.bank.bo;

import com.cgi.bank.utils.EventType;
import lombok.Data;

import jakarta.persistence.*;

@Data
@Entity
public class Audit {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  @Column(length = 100)
  private String message;

  @Enumerated(EnumType.STRING)
  private EventType eventType;

}
