package com.cgi.bank.bo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;


@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity

public class UserEntity {

  public UserEntity(String username, String password, String gender, String lastname, String firstname, Date birthdate, Role role) {
    this.username = username;
    this.password = password;
    this.gender = gender;
    this.lastname = lastname;
    this.firstname = firstname;
    this.birthdate = birthdate;
    this.role = role;
  }

  public UserEntity(String username, String password) {
    this.username = username;
    this.password = password;
  }

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  @Column(length = 10, nullable = false, unique = true)
  private String username;

  @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
  private String password;

  @Column(length = 10, nullable = false)
  private String gender;

  @Column(length = 60, nullable = false)
  private String lastname;

  @Column(length = 60, nullable = false)
  private String firstname;

  @Temporal(TemporalType.DATE)
  private Date birthdate;

  @ManyToOne(fetch = FetchType.EAGER)
  private Role role;

  @JsonIgnore
  @OneToMany(fetch = FetchType.LAZY,mappedBy = "user")
  private Collection<BankAccount> bankAccounts = new ArrayList<>();


}
