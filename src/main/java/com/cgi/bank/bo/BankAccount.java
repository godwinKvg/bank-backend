package com.cgi.bank.bo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Data;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;

@Data
@Entity
public class BankAccount {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  @Column(length = 16, unique = true)
  private String accountNumber;

  private String rib;

  @Column(precision = 16, scale = 2)
  private BigDecimal balance;

  @ManyToOne
  @JoinColumn(nullable = false)
  private UserEntity user;

  @JsonIgnore
  @OneToMany(fetch = FetchType.LAZY,mappedBy = "senderBankAccount")
  private Collection<Transfer> transfers = new ArrayList<>();

  @JsonIgnore
  @OneToMany(fetch = FetchType.LAZY,mappedBy = "operationAccount")
  private Collection<Operation> operations = new ArrayList<>();

}
