package com.cgi.bank.bo;

import com.cgi.bank.utils.OperationType;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;



@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class Operation {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(precision = 16, scale = 2, nullable = false)
    private BigDecimal amount;

    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateExecution;

    @Enumerated(EnumType.STRING)
    private OperationType operationType;

    @Column
    private String operationAuthorName;

    @ManyToOne
    private BankAccount operationAccount;

    @Column(length = 200)
    private String reason;
}
