package com.cgi.bank.dto;

import com.cgi.bank.bo.Role;
import lombok.Data;

import java.util.Date;

@Data
public class UserEntityDto {
    private Long id;
    private String username;
    private String password;
    private String gender;
    private String lastname;
    private String firstname;
    private Date birthdate;
    private Role role;
}
