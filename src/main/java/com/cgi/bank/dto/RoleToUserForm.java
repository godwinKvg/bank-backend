package com.cgi.bank.dto;

import lombok.Data;

@Data
public class RoleToUserForm {
    private String username;
    private String rolename;
}
