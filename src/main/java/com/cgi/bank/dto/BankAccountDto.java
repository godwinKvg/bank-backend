package com.cgi.bank.dto;

import lombok.Data;

import java.math.BigDecimal;
@Data
public class BankAccountDto {
    private Long id;
    private String accountNumber;
    private String rib;
    private BigDecimal balance;
    private Long userId;
    private String username;
}
