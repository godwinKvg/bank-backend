package com.cgi.bank.dto;

import lombok.Data;

@Data
public class TransferDto extends TransactionDto {
  private String senderAccountNumber;
  private String receiverAccountNumber;
}
