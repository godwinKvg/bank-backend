package com.cgi.bank.dto;

import lombok.Data;

import java.util.List;

@Data
public class OperationListDTO {
    private int currentPage;
    private int totalPages;
    private int pageSize;
    private List<OperationDto> operationDtos;
}