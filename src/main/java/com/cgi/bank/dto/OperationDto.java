package com.cgi.bank.dto;

import com.cgi.bank.utils.OperationType;
import lombok.Data;

@Data
public class OperationDto extends TransactionDto {
    private String accountNumber;
    private String operationAuthorName;
    private OperationType operationType;
}
