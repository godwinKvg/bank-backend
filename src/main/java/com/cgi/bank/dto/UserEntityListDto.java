package com.cgi.bank.dto;

import lombok.Data;

import java.util.List;

@Data
public class UserEntityListDto {
    private int currentPage;
    private int totalPages;
    private int pageSize;
    private List<UserEntityDto> usersDto;
}
