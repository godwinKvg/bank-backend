package com.cgi.bank.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public abstract class TransactionDto {
    private String reason;
    private BigDecimal amount;
    private Date date;
}
