package com.cgi.bank.dto;

import lombok.Data;

import java.util.List;

@Data
public class BankAccountListDTO {
    private int currentPage;
    private int totalPages;
    private int pageSize;
    private List<BankAccountDto> bankAccountDtos;
}
