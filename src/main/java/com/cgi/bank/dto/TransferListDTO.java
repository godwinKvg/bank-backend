package com.cgi.bank.dto;

import lombok.Data;

import java.util.List;

@Data
public class TransferListDTO {
    private String accountNumber;
    private int currentPage;
    private int totalPages;
    private int pageSize;
    private List<TransferDto> transferDtos;
}
