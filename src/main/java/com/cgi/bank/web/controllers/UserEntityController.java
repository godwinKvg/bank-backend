package com.cgi.bank.web.controllers;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.cgi.bank.bo.Role;
import com.cgi.bank.bo.UserEntity;
import com.cgi.bank.dto.RoleToUserForm;
import com.cgi.bank.dto.UserEntityDto;
import com.cgi.bank.dto.UserEntityListDto;
import com.cgi.bank.exceptions.UserAlreadyExistsException;
import com.cgi.bank.services.UserEntityService;
import com.cgi.bank.utils.JWTUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("users")
public class UserEntityController {
    private static final Logger log = LoggerFactory.getLogger(UserEntityController.class);
    private final UserEntityService userEntityService;

    public UserEntityController(UserEntityService accountService) {
        this.userEntityService = accountService;
    }

    @GetMapping
    List<UserEntity> appUsers(){
        return  userEntityService.listUsers();
    }

    @GetMapping("pagination")
    UserEntityListDto getUsers(@RequestParam(name="page",defaultValue = "0") int page,
                               @RequestParam(name="size",defaultValue = "10") int size){
        return userEntityService.getUsers(page,size);
    }




    @PostMapping("create")
    UserEntity saveUser(@RequestBody UserEntity u) throws UserAlreadyExistsException {
        return userEntityService.createUser(u);
    }

    @PostMapping("roles")
    Role saveRole (@RequestBody Role r){
        return userEntityService.addNewRole(r);
    }

    @PostMapping("addRoleToUser")
    void addRoleToUser(@RequestBody RoleToUserForm ru){
        userEntityService.addRoleToUser(ru.getUsername(),ru.getRolename());
    }


    @PutMapping("update")
    UserEntity updateUser(@RequestBody UserEntityDto u){
        return userEntityService.updateUser(u);
    }


    @PostMapping("changePassword")
    UserEntity changePassword(@RequestBody UserEntity u){
        return userEntityService.changePassword(u);
    }



    @GetMapping("refreshToken")
    void refreshToken(HttpServletRequest request, HttpServletResponse response) throws Exception{

        String jwtAuthorizationToken = request.getHeader(JWTUtils.AUTH_HEADER);

        if (jwtAuthorizationToken!=null && jwtAuthorizationToken.startsWith(JWTUtils.PREFIX)){
            try {
                // La vérification du token génère des exceptions c'est pr cela nous avons besoin du bloc try...catch

                String jwt = jwtAuthorizationToken.substring(JWTUtils.PREFIX.length());
                Algorithm algorithm = Algorithm.HMAC256(JWTUtils.SECRET);

                // Vérification et récupération des informations du token
                JWTVerifier jwtVerifier = JWT.require(algorithm).build();
                DecodedJWT decodedJWT = jwtVerifier.verify(jwt);
                String username = decodedJWT.getSubject();

                UserEntity user = userEntityService.loadUserByUsername(username);

                String jwtAccessToken = JWT.create()
                        .withSubject(user.getUsername())
                        .withExpiresAt(new Date(System.currentTimeMillis() + JWTUtils.EXPIRE_ACCESS_TOKEN))
                        .withIssuer(request.getRequestURL().toString())
                        .withClaim("roles", user.getRole().getRoleName()).sign(algorithm);


                Map<String,String> idToken = new HashMap<>();
                idToken.put("access-token",jwtAccessToken);
                idToken.put("expires-at",String.valueOf(JWTUtils.EXPIRE_ACCESS_TOKEN));
                idToken.put("refresh-token",jwt);

                response.setContentType("application/json");
                new ObjectMapper().writeValue(response.getOutputStream(),idToken);

            }catch (Exception e){
                throw e;
            }

        }
        else{
            throw new RuntimeException("Refresh token required");
        }


        }


    // Comment connaître l'utilisateur authentifié
    @GetMapping("profile")
    UserEntity getProfile(Authentication authentication){
        UserEntity u = userEntityService.loadUserByUsername(authentication.getName());
        u.setRole(u.getRole());
        return u;
    }


}
