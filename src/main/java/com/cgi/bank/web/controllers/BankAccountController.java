package com.cgi.bank.web.controllers;

import com.cgi.bank.bo.BankAccount;
import com.cgi.bank.dto.BankAccountDto;
import com.cgi.bank.dto.BankAccountListDTO;
import com.cgi.bank.exceptions.AccountNotFoundException;
import com.cgi.bank.exceptions.BankAccountNotFound;
import com.cgi.bank.exceptions.MaximumNumberOfAccountReached;
import com.cgi.bank.services.BankAccountService;
import com.cgi.bank.utils.RandomStringGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RequestMapping("/bankAccounts")
@RestController
public class BankAccountController {

    private static final Logger log = LoggerFactory.getLogger(BankAccountController.class);
    BankAccountService bankAccountService;

    public BankAccountController(BankAccountService bankAccountService) {
        this.bankAccountService = bankAccountService;
    }

    @GetMapping
    BankAccountListDTO getAccounts(@RequestParam(name="page",defaultValue = "0") int page,
                                         @RequestParam(name="size",defaultValue = "5") int size){
        return bankAccountService.getAccounts(page,size);
    }

    @GetMapping("my")
    List<BankAccount> getMyAccounts(Authentication authenticatedUser) {
        return bankAccountService.getAccountsByUsername(authenticatedUser.getName());
    }

    @GetMapping("username/{username}")
    BankAccountListDTO getAccountsByUsername(@PathVariable String username,@RequestParam(name="page",defaultValue = "0") int page,
                                             @RequestParam(name="size",defaultValue = "5") int size ) {

        return bankAccountService.getAccountsByUsernameWithPagination(username,page, size);
    }

    @GetMapping("{accountNumber}")
    BankAccount getAccount(@PathVariable String accountNumber) throws BankAccountNotFound {
        return bankAccountService.getAccount(accountNumber);
    }


    @PostMapping("create")
    BankAccount create(@RequestBody BankAccountDto bankAccountDto,Authentication authentication) throws MaximumNumberOfAccountReached {
        if(bankAccountDto.getUsername()==null)
            bankAccountDto.setUsername(authentication.getName());

        return bankAccountService.create(bankAccountDto);
    }


    @PostMapping("update")
    BankAccount update(@RequestBody BankAccount bankAccount) throws AccountNotFoundException {
        return bankAccountService.update(bankAccount);
    }
}
