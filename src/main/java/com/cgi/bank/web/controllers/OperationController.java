package com.cgi.bank.web.controllers;

import com.cgi.bank.bo.Operation;
import com.cgi.bank.dto.OperationDto;
import com.cgi.bank.dto.OperationListDTO;
import com.cgi.bank.exceptions.AccountNotFoundException;
import com.cgi.bank.exceptions.InsuffisantBalanceException;
import com.cgi.bank.exceptions.TransactionException;
import com.cgi.bank.services.OperationService;
import com.cgi.bank.utils.ResponseMessage;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("operations")
public class OperationController {

    OperationService operationService;

    public OperationController(OperationService operationService) {
        this.operationService = operationService;
    }

    // Mix
    @GetMapping
    List<Operation> getOperations() {
        return operationService.getAll();
    }

    @GetMapping("{accountNumber}")
    OperationListDTO getAllByAccountNumber(@PathVariable String accountNumber,@RequestParam(name="page",defaultValue = "0") int page,
                                           @RequestParam(name="size",defaultValue = "5") int size) {
        return operationService.getMyOperations(accountNumber, page, size);
    }

    // Withdrawals
    @GetMapping("withdrawals")
    List<Operation> getWithdrawals() {
        return operationService.getWithdrawals();
    }

    @GetMapping("withdrawals/my/{username}")
    List<Operation> getMyWithdrawals(@PathVariable String username) {
        return operationService.getMyWithdrawals(username);
    }

    @GetMapping("withdrawals/{accountNumber}")
    List<Operation> getWithdrawalsByAccountNumber(@PathVariable String accountNumber) {
        return operationService.getWithdrawalsByAccountNumber(accountNumber);
    }

    @PostMapping("withdrawals")
    ResponseEntity<ResponseMessage> createWithdrawal(@RequestBody OperationDto operationDto) throws AccountNotFoundException, InsuffisantBalanceException, TransactionException {
        operationService.createWithdrawal(operationDto);
        return new ResponseEntity<>(new ResponseMessage("Opération réussie"), HttpStatus.OK);
    }

    // Deposits
    @GetMapping("deposits")
    List<Operation> getDeposits() {
        return operationService.getDeposits();
    }

    @GetMapping("deposits/{accountNumber}")
    List<Operation> getDepositsByAccountNumber(@PathVariable String accountNumber) {
        return operationService.getDeposits();
    }

    @GetMapping("deposits/my/{username}")
    List<Operation> getMyDeposits(@PathVariable String username) {
        return operationService.getMyWithdrawals(username);
    }

    @PostMapping("deposits")
    ResponseEntity<ResponseMessage> createDeposit(@RequestBody OperationDto operationDto) throws AccountNotFoundException, InsuffisantBalanceException, TransactionException {
        operationService.createDeposit(operationDto);
        return new ResponseEntity<>(new ResponseMessage("Opération réussie"), HttpStatus.CREATED);
    }
}
