package com.cgi.bank.web.controllers;
import com.cgi.bank.bo.Transfer;
import com.cgi.bank.dto.TransferDto;
import com.cgi.bank.dto.TransferListDTO;
import com.cgi.bank.exceptions.AccountNotFoundException;
import com.cgi.bank.exceptions.InsuffisantBalanceException;
import com.cgi.bank.exceptions.TransactionException;
import com.cgi.bank.services.TransferService;
import com.cgi.bank.utils.ResponseMessage;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController()
@RequestMapping("transfers")
public
class TransferController {

    private final TransferService transferService;

    public TransferController(TransferService transferService) {
        this.transferService = transferService;
    }


    @GetMapping
    List<Transfer> getTransfers() {
        return transferService.findAll();
    }

    @GetMapping("{accountNumber}")
    TransferListDTO getMyTransfersByAccountNumber(@PathVariable String accountNumber,
                                                  @RequestParam(name="page",defaultValue = "0") int page,
                                                  @RequestParam(name="size",defaultValue = "5") int size) {
        return transferService.getMyTransfersByAccountNumber(accountNumber, page, size);
    }


    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<ResponseMessage> createTransfer(@RequestBody TransferDto transferDto)
            throws InsuffisantBalanceException, AccountNotFoundException, TransactionException {
       transferService.createTransfer(transferDto);
        return new ResponseEntity<>(new ResponseMessage("Opération réussie"), HttpStatus.CREATED);
    }
}
