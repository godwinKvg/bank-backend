package com.cgi.bank.web.common;

import com.cgi.bank.exceptions.*;
import com.cgi.bank.utils.ResponseMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.resource.NoResourceFoundException;

@ControllerAdvice
public class ExceptionHandlingController {

    private static final Logger log = LoggerFactory.getLogger(ExceptionHandlingController.class);

    @ExceptionHandler(InsuffisantBalanceException.class)
    public ResponseEntity<ResponseMessage> handleInsuffisantBalanceException(InsuffisantBalanceException ex, WebRequest request) {
        log.error("Insufficient balance:", ex);
        ResponseMessage errorMessage = new ResponseMessage(ex.getMessage());
        return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(BankAccountNotFound.class)
    public ResponseEntity<ResponseMessage> handleBankAccountNotFoundException(BankAccountNotFound ex, WebRequest request) {
        log.error("Insufficient balance:", ex);
        ResponseMessage errorMessage = new ResponseMessage(ex.getMessage());
        return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(MaximumNumberOfAccountReached.class)
    public ResponseEntity<ResponseMessage> handleMaximumAccountsReachedException(MaximumNumberOfAccountReached ex, WebRequest request) {
        log.error("Maximum account reached:", ex);
        ResponseMessage errorMessage = new ResponseMessage(ex.getMessage());
        return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(AccountNotFoundException.class)
    public ResponseEntity<ResponseMessage> handleAccountNotFoundException(AccountNotFoundException ex, WebRequest request) {
        log.error("Account not found:", ex);
        ResponseMessage errorMessage = new ResponseMessage(ex.getMessage());
        return new ResponseEntity<>(errorMessage, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(TransactionException.class)
    public ResponseEntity<ResponseMessage> handleTransactionException(TransactionException ex, WebRequest request) {
        log.error("Transaction exception:", ex);
        ResponseMessage errorMessage = new ResponseMessage(ex.getMessage());
        return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(UserAlreadyExistsException.class)
    public ResponseEntity<ResponseMessage> handleUserAlreadyExistsException(UserAlreadyExistsException ex, WebRequest request) {
        log.error("User already exists :", ex);
        ResponseMessage errorMessage = new ResponseMessage(ex.getMessage());
        return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
    }


    @ExceptionHandler(UsernameNotFoundException.class)
    public ResponseEntity<ResponseMessage> handleIncorrectAuthException(UsernameNotFoundException ex, WebRequest request) {
        log.error("User not found:", ex);
        ResponseMessage errorMessage = new ResponseMessage("User not found.");
        return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(AccessDeniedException.class)
    public ResponseEntity<ResponseMessage> handleAuthorizationException(AccessDeniedException ex, WebRequest request) {
        log.error("Authorization exception:", ex);
        ResponseMessage errorMessage = new ResponseMessage("Access denied.");
        return new ResponseEntity<>(errorMessage, HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(AuthenticationException.class)
    public ResponseEntity<ResponseMessage> handleAuthenticationException(AuthenticationException ex, WebRequest request) {
        log.error("Authentication exception:", ex);
        ResponseMessage errorMessage = new ResponseMessage("Authentication required.");
        return new ResponseEntity<>(errorMessage, HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler({NoResourceFoundException.class, HttpRequestMethodNotSupportedException.class})
    public ResponseEntity<ResponseMessage> handleResourceNotFoundException(Exception ex, WebRequest request) {
        log.error("Resource not found:", ex);
        ResponseMessage errorMessage = new ResponseMessage("Resource not found.");
        return new ResponseEntity<>(errorMessage, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ResponseMessage> handleAnyException(Exception ex, WebRequest request) {
        log.error("An error occurred:", ex);
        ResponseMessage errorMessage = new ResponseMessage("An error occurred. Please try again.");
        return new ResponseEntity<>(errorMessage, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
