package com.cgi.bank.utils;


import com.cgi.bank.bo.BankAccount;
import com.cgi.bank.dto.OperationDto;
import com.cgi.bank.dto.TransferDto;
import com.cgi.bank.exceptions.AccountNotFoundException;
import com.cgi.bank.exceptions.InsuffisantBalanceException;
import com.cgi.bank.exceptions.TransactionException;
import com.cgi.bank.services.TransferService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;


public class TransactionValidator {
    static Logger LOGGER = LoggerFactory.getLogger(TransferService.class);

    private static final int MONTANT_MAXIMAL = 40000;
    private static final int MONTANT_MINIMAL = 10;

    public static void validateTransfert(BankAccount senderBankAccount, BankAccount recieverBankAccount, TransferDto transferDto)throws InsuffisantBalanceException, AccountNotFoundException, TransactionException {

        if (senderBankAccount == null) {
            LOGGER.error("Compte de l'emetteur Non existant");
            throw new AccountNotFoundException("Compte de l'emetteur Non existant");
        }

        if (recieverBankAccount == null) {
            LOGGER.error("Compte du receveur Non existant");
            throw new AccountNotFoundException("Compte du receveur Non existant");
        }

        if (transferDto.getAmount() == null || transferDto.getAmount().intValue() == 0) {
            LOGGER.error("Montant vide");
            throw new TransactionException("Montant vide");

        } else if (transferDto.getAmount().intValue() < MONTANT_MINIMAL) {
            LOGGER.error("Montant minimal de transfer non atteint");
            throw new TransactionException("Montant minimal de transfer non atteint");


        } else if (transferDto.getAmount().intValue() > MONTANT_MAXIMAL) {
            LOGGER.error("Montant maximal de transfer dépassé");
            throw new TransactionException("Montant maximal de transfer dépassé");
        }

        if (transferDto.getReason() == null || transferDto.getReason().isEmpty()) {
            LOGGER.error("Motif vide");
            throw new TransactionException("Motif vide");
        }

        if (senderBankAccount.getBalance().intValue() - transferDto.getAmount().intValue() < 0) {
            LOGGER.error("Solde insuffisant pour l'emetteur");
            throw new InsuffisantBalanceException();
        }
    }

    public static void validateOperation(int numberOfOperationOfToday, BankAccount bankAccount, OperationDto operationDto) throws TransactionException,AccountNotFoundException, InsuffisantBalanceException{
        if (numberOfOperationOfToday>10){
            throw new TransactionException("Nombre maximal de transaction effectué");
        }

        if (bankAccount==null){
            LOGGER.warn("Compte Non existant");
            throw new AccountNotFoundException();
        }
        if (operationDto.getAmount().intValue() > MONTANT_MAXIMAL){
                LOGGER.warn("Montant maximal dépassé");
                throw new TransactionException("Montant maximal dépassé");
        }
        if (StringUtils.isEmpty(operationDto.getOperationAuthorName())){
            LOGGER.warn("Nom de l'auteur de l'opération non fourni");
            throw new TransactionException("Nom de l'auteur de l'opération non fourni");
        }
        if (operationDto.getAmount().intValue() > bankAccount.getBalance().intValue() && operationDto.getOperationType().equals(OperationType.WITHDRAWAL)){
            LOGGER.error("Solde insuffisant");
            throw new InsuffisantBalanceException();
        }
        if (StringUtils.isEmpty(operationDto.getReason())) {
            LOGGER.warn("Motif vide");
            throw new TransactionException("Motif vide");
        }
    }


}
