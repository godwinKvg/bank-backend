package com.cgi.bank.utils;

public enum EventType {

  TRANSFER("Money Tranfer"),
  DEPOSIT("Money Deposit"),
  WITHDRAWAL("Money Withdrawal");


  private String type;

  EventType(String type) {
    this.type = type;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }
}
