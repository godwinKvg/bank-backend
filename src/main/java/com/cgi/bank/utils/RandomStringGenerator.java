package com.cgi.bank.utils;

import java.util.UUID;

public class RandomStringGenerator {
        public static String generateString() {
            String uuid = UUID.randomUUID().toString();
            return uuid.replace("-","").toUpperCase().substring(0,16);
        }
}
