package com.cgi.bank.utils;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class JWTUtils {

    public static String SECRET;
    public static final String AUTH_HEADER="Authorization";
    public static final String PREFIX="Bearer ";

    public static final long EXPIRE_ACCESS_TOKEN=50*60*1000;
    public static final long EXPIRE_REFRESH_TOKEN=24*60*60*1000;

    @Value("${jwtutils.secret}")
    private void setSECRET(String s){
        SECRET=s;
    }


}
