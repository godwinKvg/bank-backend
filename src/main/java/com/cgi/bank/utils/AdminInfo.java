package com.cgi.bank.utils;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.Date;

@Data
@Component
@ConfigurationProperties(prefix = "admin")
public class AdminInfo {

    private String username;

    private String password;

    private String gender;

    private String lastname;

    private String firstname;

    private Date birthdate;
}
