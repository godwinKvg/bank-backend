package com.cgi.bank;

import com.cgi.bank.bo.*;
import com.cgi.bank.mapper.OperationMapper;
import com.cgi.bank.mapper.TransferMapper;
import com.cgi.bank.services.UserEntityService;
import com.cgi.bank.services.BankAccountService;
import com.cgi.bank.services.OperationService;
import com.cgi.bank.services.TransferService;
import com.cgi.bank.utils.AdminInfo;
import com.cgi.bank.utils.OperationType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.math.BigDecimal;
import java.util.Date;


@SpringBootApplication
public class BankApplication {

	@Autowired
	private AdminInfo adminInfo;

	public static void main(String[] args) {
		SpringApplication.run(BankApplication.class, args);
	}


	@Bean
	public WebMvcConfigurer corsConfigurer() {
		return new WebMvcConfigurer() {
			@Override
			public void addCorsMappings(CorsRegistry registry) {
				registry.addMapping("/**")
						.allowedOrigins("http://localhost:4200")
						.allowedMethods("GET", "POST", "PUT", "DELETE")
						.allowedHeaders("Authorization", "Content-Type")
						.maxAge(1800);
			}
		};
	}

	@Bean
	CommandLineRunner start(UserEntityService userEntityService, BankAccountService bankAccountService, TransferService transferService, OperationService operationService) {

		return args -> {


			// TODO : Should be remove and implemented as a migration using flyway
			userEntityService.addNewRole(new Role(null,"ADMIN"));
			userEntityService.addNewRole(new Role(null,"USER"));

			UserEntity userEntity = new UserEntity();
			if ( userEntityService.loadUserByUsername(adminInfo.getUsername()) == null) {

				userEntity.setUsername(adminInfo.getUsername());
				userEntity.setPassword(adminInfo.getPassword());
				userEntity.setGender(adminInfo.getGender()!=null?adminInfo.getGender():"");
				userEntity.setBirthdate(adminInfo.getBirthdate()!=null?adminInfo.getBirthdate():new Date());
				userEntity.setLastname(adminInfo.getLastname()!=null?adminInfo.getLastname():"");
				userEntity.setFirstname(adminInfo.getFirstname()!=null?adminInfo.getFirstname():"");

				UserEntity admin = userEntityService.createUser(userEntity);

				userEntityService.addRoleToUser(admin.getUsername(), "ADMIN");

			}


			// Saving users for test purpose
			// TODO : Should be deleted before production

			UserEntity appUser1 = userEntityService.createUser(new UserEntity("user1","1234","Male","last1","first1",new Date(),null));
			UserEntity appUser2 = userEntityService.createUser(new UserEntity("user2","1234","Female","last2","first1",new Date(),null));


			BankAccount bankAccount2 = new BankAccount();
			bankAccount2.setAccountNumber("010000B025001000");
			bankAccount2.setRib("RIB2");
			bankAccount2.setBalance(BigDecimal.valueOf(140000L));
			bankAccount2.setUser(appUser2);

			bankAccount2 = bankAccountService.save(bankAccount2);

			for (int i = 0; i < 8; i++) {
				BankAccount bankAccount1 = new BankAccount();
				bankAccount1.setAccountNumber("010000A000000"+ i);
				bankAccount1.setRib("RIB1");
				bankAccount1.setBalance(BigDecimal.valueOf(200000L));
				bankAccount1.setUser(appUser1);


				bankAccount1 = bankAccountService.save(bankAccount1);

				Transfer transfer = new Transfer();
				transfer.setReason("Transfer !"+i);
				transfer.setAmount(BigDecimal.valueOf(2000));
				transfer.setReceiverBankAccount(bankAccount2);
				transfer.setSenderBankAccount(bankAccount1);
				transfer.setDateExecution(new Date());

				transferService.createTransfer(TransferMapper.mapModelToDto(transfer));

				for (int j = 0; j < 5; j++) {


					Operation operation = new Operation();
					if (j%2==0){
						operation.setOperationType(OperationType.DEPOSIT);
					}else{
						operation.setOperationType(OperationType.WITHDRAWAL);
					}
					operation.setReason("Depot d'argent pour urgence !");
					operation.setAmount(BigDecimal.valueOf(j+ 2000L));
					operation.setOperationAccount(bankAccount1);
					operation.setOperationAuthorName("Aboudou");
					operation.setDateExecution(new Date());

					operationService.createDeposit(OperationMapper.mapModelToDto(operation));
				}

			}

			BankAccount bankAccount3 = new BankAccount();
			bankAccount3.setAccountNumber("0100A00025001000");
			bankAccount3.setRib("RIB2");
			bankAccount3.setBalance(BigDecimal.valueOf(140000L));
			bankAccount3.setUser(appUser2);

			bankAccountService.save(bankAccount3);



		};
	}


}
