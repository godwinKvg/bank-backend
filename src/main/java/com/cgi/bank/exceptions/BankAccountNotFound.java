package com.cgi.bank.exceptions;

public class BankAccountNotFound extends Exception{
    private static final long serialVersionUID = 1L;

    public BankAccountNotFound() {
        super("Bank Account Not Found");
    }

}

