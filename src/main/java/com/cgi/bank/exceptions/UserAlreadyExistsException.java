package com.cgi.bank.exceptions;

public class UserAlreadyExistsException extends Exception {

  private static final long serialVersionUID = 1L;

  public UserAlreadyExistsException() {
    super("Utilisateur existe déjà");
  }
  public UserAlreadyExistsException(String message){
    super(message);
  }
}
