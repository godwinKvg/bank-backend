package com.cgi.bank.exceptions;

public class InsuffisantBalanceException extends Exception {

  private static final long serialVersionUID = 1L;

  public InsuffisantBalanceException() {
    super("Solde insuffisant");
  }
}
