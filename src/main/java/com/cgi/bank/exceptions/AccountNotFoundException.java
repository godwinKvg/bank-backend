package com.cgi.bank.exceptions;

public class AccountNotFoundException extends Exception {

  private static final long serialVersionUID = 1L;

  public AccountNotFoundException() {
    super("Compte Non existant");
  }
  public AccountNotFoundException(String message){
    super(message);
  }
}
