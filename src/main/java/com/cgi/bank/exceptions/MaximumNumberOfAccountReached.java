package com.cgi.bank.exceptions;

public class MaximumNumberOfAccountReached extends Exception{
    private static final long serialVersionUID = 1L;

    public MaximumNumberOfAccountReached() {
        super("maximum number of accounts reached");
    }
}
