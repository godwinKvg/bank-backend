package com.cgi.bank.mapper;

import com.cgi.bank.bo.BankAccount;
import com.cgi.bank.bo.Transfer;
import com.cgi.bank.dto.TransferDto;
import lombok.Data;

import java.util.Date;

@Data
public class TransferMapper {

    private static TransferDto transferDto;

    public static TransferDto mapModelToDto(Transfer transfer) {
        transferDto = new TransferDto();
        transferDto.setSenderAccountNumber(transfer.getSenderBankAccount().getAccountNumber());
        transferDto.setDate(transfer.getDateExecution());
        transferDto.setReason(transfer.getReason());
        transferDto.setAmount(transfer.getAmount());
        transferDto.setReceiverAccountNumber(transfer.getReceiverBankAccount().getAccountNumber());
        transferDto.setDate(transfer.getDateExecution());
        return transferDto;

    }

    public static Transfer mapDtoToModel(TransferDto transferDto, BankAccount recieverBankAccount, BankAccount senderBankAccount){
        Transfer transfer = new Transfer();
        transfer.setDateExecution(transferDto.getDate());
        transfer.setReceiverBankAccount(recieverBankAccount);
        transfer.setSenderBankAccount(senderBankAccount);
        transfer.setAmount(transferDto.getAmount());
        transfer.setReason(transferDto.getReason());
        transfer.setDateExecution(transferDto.getDate() != null ? transferDto.getDate() : new Date());

        return  transfer;
    }
}
