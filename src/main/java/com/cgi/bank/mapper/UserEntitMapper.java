package com.cgi.bank.mapper;

import com.cgi.bank.bo.UserEntity;
import com.cgi.bank.dto.UserEntityDto;

public class UserEntitMapper {
    public static UserEntity mapDtoToModel(UserEntityDto userEntityDto) {
        UserEntity userEntity = new UserEntity();
        userEntity.setId(userEntityDto.getId());
        userEntity.setRole(userEntityDto.getRole());
        userEntity.setUsername(userEntityDto.getUsername());
        userEntity.setFirstname(userEntityDto.getFirstname());
        userEntity.setLastname(userEntityDto.getLastname());
        userEntity.setBirthdate(userEntityDto.getBirthdate());
        return userEntity;
    }


    public static UserEntityDto mapModelToDto(UserEntity userEntity) {
        UserEntityDto userEntityDto = new UserEntityDto();
        userEntityDto.setId(userEntity.getId());
        userEntityDto.setRole(userEntity.getRole());
        userEntityDto.setUsername(userEntity.getUsername());
        userEntityDto.setFirstname(userEntity.getFirstname());
        userEntityDto.setLastname(userEntity.getLastname());
        userEntityDto.setBirthdate(userEntity.getBirthdate());
        return userEntityDto;
    }
}
