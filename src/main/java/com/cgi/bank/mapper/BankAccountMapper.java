package com.cgi.bank.mapper;

import com.cgi.bank.bo.BankAccount;
import com.cgi.bank.bo.UserEntity;
import com.cgi.bank.dto.BankAccountDto;


public class BankAccountMapper {

    public static BankAccount mapDtoToModel(BankAccountDto bankAccountDto, UserEntity user){
        BankAccount account = new BankAccount();
        account.setAccountNumber(bankAccountDto.getAccountNumber());
        account.setBalance(bankAccountDto.getBalance());
        account.setRib(bankAccountDto.getRib());
        account.setUser(user);
        account.setId(bankAccountDto.getId());
        return account;
    }

    public static BankAccountDto mapModelToDto(BankAccount bankAccount){
        BankAccountDto account = new BankAccountDto();
        account.setUserId(bankAccount.getId());
        account.setBalance(bankAccount.getBalance());
        account.setRib(bankAccount.getRib());
        account.setUserId(bankAccount.getUser().getId());
        account.setAccountNumber(bankAccount.getAccountNumber());
        account.setId(bankAccount.getId());
        return account;
    }


}
