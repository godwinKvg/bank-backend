package com.cgi.bank.mapper;

import com.cgi.bank.bo.BankAccount;
import com.cgi.bank.bo.Operation;

import com.cgi.bank.dto.OperationDto;
import lombok.Data;

import java.util.Date;

@Data
public class OperationMapper {
    private static OperationDto operationDto;

    public static OperationDto mapModelToDto(Operation operation) {
        operationDto = new OperationDto();
        operationDto.setAccountNumber(operation.getOperationAccount().getAccountNumber());
        operationDto.setDate(operation.getDateExecution());
        operationDto.setReason(operation.getReason());
        operationDto.setAmount(operation.getAmount());
        operationDto.setOperationAuthorName(operation.getOperationAuthorName());
        operationDto.setOperationType(operation.getOperationType());

        return operationDto;

    }

    public static Operation mapDtoToModel(OperationDto operationDto, BankAccount bankAccount){
        Operation operation = new Operation();
        operation.setReason(operationDto.getReason());
        operation.setAmount(operationDto.getAmount());
        operation.setOperationAccount(bankAccount);
        operation.setOperationAuthorName(bankAccount.getUser().getUsername());
        operation.setDateExecution(operationDto.getDate() == null ? new Date():operationDto.getDate());
        return operation;
    }
}
