package com.cgi.bank.repository;

import com.cgi.bank.bo.BankAccount;
import com.cgi.bank.bo.Operation;
import com.cgi.bank.utils.OperationType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;


public interface OperationRepository extends JpaRepository<Operation,Long> {
    int countAllByOperationAccountAndDateExecution(BankAccount bankAccount, Date date);
    Page<Operation> getAllByOperationAccount_AccountNumberOrderByDateExecutionDesc(String accountNumber, Pageable pageable);
    List<Operation> getAllByOperationType(OperationType operationType);
    List<Operation> getAllByOperationTypeAndOperationAccount_User_Username(OperationType operationType, String username);

    List<Operation> getAllByOperationAccount_AccountNumber(String accountNumber);

    List<Operation> findAllByOperationAccount_AccountNumberAndOperationType(String accountNumber, OperationType operationType);
}
