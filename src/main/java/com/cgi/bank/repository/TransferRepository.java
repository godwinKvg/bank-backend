package com.cgi.bank.repository;

import com.cgi.bank.bo.Transfer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TransferRepository extends JpaRepository<Transfer, Long> {
    List<Transfer> findAllBySenderBankAccount_AccountNumber(String accountNumber);
Page<Transfer> getAllBySenderBankAccount_AccountNumberOrReceiverBankAccount_AccountNumberOrderByDateExecutionDesc(String senderAccountNumber,String receiverAccountNumber, Pageable pageable);
}
