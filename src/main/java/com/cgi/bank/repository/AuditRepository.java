package com.cgi.bank.repository;

import com.cgi.bank.bo.Audit;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AuditRepository extends JpaRepository<Audit, Long> {
}
