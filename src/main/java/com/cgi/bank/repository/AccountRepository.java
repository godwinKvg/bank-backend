package com.cgi.bank.repository;

import com.cgi.bank.bo.BankAccount;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.data.domain.Pageable;

import java.util.List;

public interface AccountRepository extends JpaRepository<BankAccount, Long> {
  BankAccount findByRib(String rib);
  BankAccount findByAccountNumber(String accountNumber);
  Page<BankAccount> findAllByUser_Username(String username, Pageable pageable);
  Page<BankAccount> findAll(Pageable pageable);
  List<BankAccount> findAllByUser_Username(String username);
  long countByUser_Username(String username);
}
