package com.cgi.bank.filters;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.cgi.bank.utils.ResponseMessage;
import com.cgi.bank.utils.JWTUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

public class JwtAuthorisationFilter extends OncePerRequestFilter {
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        if (request.getServletPath().equals("/users/refreshToken")) {
            filterChain.doFilter(request,response);
        } else {
            String jwtAuthorizationToken = request.getHeader(JWTUtils.AUTH_HEADER);

            if (jwtAuthorizationToken != null && jwtAuthorizationToken.startsWith(JWTUtils.PREFIX)) {
                try {// La vérification du token génère des exceptions c'est pr cela nous avons besoin du bloc try...catch

                    String jwt = jwtAuthorizationToken.substring(JWTUtils.PREFIX.length());
                    Algorithm algorithm = Algorithm.HMAC256(JWTUtils.SECRET);

                    // Vérification et récupération des informations du token
                    JWTVerifier jwtVerifier = JWT.require(algorithm).build();
                    DecodedJWT decodedJWT = jwtVerifier.verify(jwt);
                    String username = decodedJWT.getSubject();
                    String[] roles = decodedJWT.getClaim("roles").asArray(String.class);

                    Collection<GrantedAuthority> authorities = new ArrayList<>();
                    Arrays.stream(roles).forEach(r -> authorities.add(new SimpleGrantedAuthority(r)));

                    // Nous allons authentifier l'utilisateur
                    // Si le token est vérifié nous n'avons plus besoin de vérifier le mot de passe
                    UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(username, null, authorities);

                    SecurityContextHolder.getContext().setAuthentication(authenticationToken);

                    filterChain.doFilter(request, response);

                } catch (Exception e) {

                    // TODO : Je veux pouvoir renvoyer cette reponse en Json mais ce que je reçois au front est
                    response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                    response.setContentType("application/json");
                    new ObjectMapper().writeValue(response.getOutputStream(),new ResponseMessage("Veuillez vous connecter à nouveau"));

//                    response.setHeader("error-message", e.getMessage());
//                    response.sendError(HttpServletResponse.SC_UNAUTHORIZED);

                }

            } else {
                filterChain.doFilter(request, response);
            }
        }
    }

}
